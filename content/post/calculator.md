---
title: Calculators
date: '2019-05-20T14:29:17-04:00'
draft: true
autoThumbnailImage: true
---

---
    Calculate Horsepower from Weight and 1/4 Mile ET

<form name="hp_calc_1" method="POST" data-netlify="true">
  <p>
    <label>Car's Race Weight: <input type="text" name="weight" /></label>   
  </p>
  <p>
    <label>1/4 Mile ET: <input type="integer" name="et" /></label>
  </p>
  
  
    <button type="submit">Submit</button>
  </p>
</form>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>
$.ajax({
   type: 'POST',
   crossDomain: 'true',
   contentType: 'application/json',
   dataType: 'json',
   url: 'https://g5sgyczhih.execute-api.us-east-1.amazonaws.com/v1',
   data: {key1: 3200, key2: 11.5},
   success: function(jsondata){

   }
})
</script>

<button>Send an HTTP POST request to a page and get the result back</button>